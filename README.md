####  Terraform layout

The aim of this guide is to explain design considerations chosen for the solution, terraform directory layout along with appropriate usage on invoking the wrapper script for deploying terraform scripts for a given environment. This repository will deploy a highly available and fault-tolerant ECS Fargate cluster and related AWS resources.

#### Cloud provider

The chosen cloud provider is **AWS** and below list of services have been used to design and implement the solution.

 - VPC
 - ECS(Fargate)
 - Autoscaling
 - S3
 - IAM
 - Cloudwatch

```bash

|
|--------envs------|
|                  |---production----|---README.md
|                  |                 |---main.tf 
|                  |                 |---variables.tf
|                  |                 |---output.tf
|                  |                 |---terraform.tfvars
|                  |                 
|                  | 
|                  |------dev--------|---README.md
|                                    |---main.tf 
|                                    |---variables.tf
|                                    |---output.tf
|                                    |---terraform.tfvars
|
|
|------modules ----|
|                  |------vpc--------|---vpc.tf
|                  |                 |---variables.tf
|                  |                 |---output.tf
|                  |                 
|                  |------s3---------|---s3.tf
|                  |                 |---bucketpolicy.tf
|                  |                 |---variables.tf
|                  |                 |---output.tf
|                  |                  
|                  |------ecs--------|---ecs_cluster.tf
|                  |                 |---ecs_service.tf
|                  |                 |---ecs_scaling.tf
|                  |                 |---task_definition.tf
|                  |                 |---security_group.tf
|                  |                 |---iam.tf
|                  |                 |---variables.tf
|                  |                 |---output.tf
|                  |                 
|                  |                 |                  
|                  |---cloudwatch----|---cloudwatch.tf
|                  |                 |---variables.tf
|                  |                 |---output.tf
|                  |
|                  |
|                  |-------ecr-------|---ecr.tf
|                  |                 |---ecr_role_policy.tf
|                  |                 |---variables.tf
|----run_tf.sh
|
|
|---README.md

```

* terraform root has two important directories - modules and envs
* envs directory contains tf vars specific to each environment
* modules directory contains various sub-modules for VPC, ECS, ECR, CloudWatch and S3 as shown in the above pictorial layout
* remote state storage has been setup for terraform using AWS S3 and AWS DynamoDB to enable better collaboration between teams
* README.md -- documentation

#### Improvements/ToDOs

* TF code review and deployment could be automated fully using Atlantis and Gitlab
* TF code could be better parameterized, for ex: the CPU and Memory metric to trigger AutoScaling can be parameterized
* More and better comments for easier understanding

