// AWS Region
aws_region = "us-east-1"

// Company
project_name = "tatum"
product_name = "python-app"

// List of ECR repos
ecr_repos_list = ["python-app"]

// Environment
environment = "dev"

// VPC Vars
vpc_cidr             = "10.10.0.0/16"
public_subnets_cidr  = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
private_subnets_cidr = ["10.10.10.0/24", "10.10.11.0/24", "10.10.12.0/24"]

//  Default tags
default_tags = {
  project_name = "tatum"
  product_name = "python-app"
}

//S3
website_configuration = true
s3_bucket_name        = "python-app-data"

// ECS
container_insights = "enabled"
services = [
  {
    "name" : "python-app",
    "container_insights" : "true",
    "desired_count" : 1,
    #"environment_files": "arn:aws:s3:::tatum-devops-env-vars/python-app.env",
    "docker_image" : "569440735201.dkr.ecr.us-east-1.amazonaws.com/python-app:26b563f9",
    "container_port" : 80,
    "container_cpu" : 256,
    "container_memory" : 512,
    "assign_public_ip" : false,
    "enable_autoscaling" : true
  }
]
