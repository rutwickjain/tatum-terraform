// Module to create S3 bucket
module "s3" {
  source                = "./modules/s3"
  environment           = var.environment
  default_tags          = var.default_tags
  aws_region            = var.aws_region
  bucket_name           = var.s3_bucket_name
  website_configuration = var.website_configuration
}
