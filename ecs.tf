# module to create ECS cluster and related resources
data "aws_caller_identity" "current" {}

module "ecs" {
  for_each                          = { for ecs_service in var.services : ecs_service.name => ecs_service }
  source                            = "./modules/ecs"
  region                            = var.aws_region
  container_insights                = var.container_insights
  project_name                      = var.project_name
  product_name                      = each.value.name
  environment                       = var.environment
  docker_image                      = each.value.docker_image
  container_port                    = each.value.container_port
  container_cpu                     = each.value.container_cpu
  container_memory                  = each.value.container_memory
  ecs_container_cloudwatch_loggroup = module.cloudwatch[each.value.name].ecs_container_cloudwatch_log_group.name
  default_tags                      = var.default_tags
  desired_count                     = each.value.desired_count
  vpc_id                            = module.vpc.vpc_id
  platform_version                  = var.platform_version
  assign_public_ip                  = each.value.assign_public_ip
  launch_type                       = var.launch_type
  private_subnets_ids               = module.vpc.private_subnets_ids
  enable_autoscaling                = each.value.enable_autoscaling
  public_subnets_ids                = module.vpc.public_subnets_ids
  s3_bucket_name                    = var.s3_bucket_name
}
