// Module to create CloudWatch resources for ECS
module "cloudwatch" {
  for_each     = toset(var.ecr_repos_list)
  source       = "./modules/cloudwatch"
  project_name = var.project_name
  environment  = var.environment
  product_name = each.key
  default_tags = var.default_tags
}
