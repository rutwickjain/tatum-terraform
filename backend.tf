terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "tf-dev-remote-state"
    key    = "tatum/terraform.tfstate"
    region = "us-east-1"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform_state_lock_db"
    encrypt        = true
  }
}

