# variables.tf

// General
variable "project_name" {}
variable "product_name" {}

// AWS Access variables
variable "aws_region" {}

// Environment
variable "environment" {
}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "2"
}

// VPC

variable "vpc_cidr" {
  description = "CIDR Block for VPC"
}

variable "public_subnets_cidr" {
  description = "CIDR Blocks for public subnets"
  type        = list(any)
}

variable "private_subnets_cidr" {
  description = "CIDR Blocks for private subnets"
  type        = list(any)
}

variable "default_tags" {
  description = "Default tags for all resources"
  type        = map(any)
}

//S3

variable "s3_bucket_name" {}
variable "website_configuration" {}

// ECS Services

variable "container_insights" {}

variable "ecr_repos_list" {}

variable "services" {
  description = "A list of selction maps"
  type        = any
  default     = []
}

variable "platform_version" {
  default = "1.4.0"
}

variable "launch_type" {
  default = "FARGATE"
}

