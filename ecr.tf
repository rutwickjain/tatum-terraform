// Module to create ECR
module "ecr" {
  source         = "./modules/ecr"
  ecr_repos_list = var.ecr_repos_list
  environment    = var.environment
  default_tags   = var.default_tags
}
