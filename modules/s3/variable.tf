#variable "bucket_acl_type" {}

variable "environment" {}

variable "default_tags" {}

variable "aws_region" {}

variable "bucket_name" {}
variable "website_configuration" {
  default = "false"
}

variable "index_document" {
  default = "index.html"

}
