// create s3 bucket
resource "aws_s3_bucket" "s3" {
  bucket = var.bucket_name
}

// configure static website for s3 bucket
resource "aws_s3_bucket_website_configuration" "s3_web" {
  count  = var.website_configuration == true ? 1 : 0
  bucket = aws_s3_bucket.s3.bucket

  index_document {
    suffix = var.index_document
  }
}
