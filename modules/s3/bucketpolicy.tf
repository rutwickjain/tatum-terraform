// create allow public access policy document
data "aws_iam_policy_document" "allow_public_access" {
  count = var.website_configuration == true ? 1 : 0
  statement {
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "${aws_s3_bucket.s3.arn}/*"
    ]
  }
}

// create s3 bucket policy for public access
resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
  count  = var.website_configuration == true ? 1 : 0
  bucket = aws_s3_bucket.s3.id
  policy = data.aws_iam_policy_document.allow_public_access[0].json
}

