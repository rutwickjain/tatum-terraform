# Create var.az_count private subnets, each in a different AZ
resource "aws_subnet" "private" {
  count = var.az_count

  vpc_id                  = aws_vpc.main.id
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = element(var.private_subnets_cidr, count.index)
  map_public_ip_on_launch = "false"

  tags = merge(
    var.default_tags,
    tomap(
      {
        "Name"        = "${var.project_name}-private-subnet-${count.index}-${var.environment}",
        "type"        = "private",
        "environment" = var.environment
      }
    )
  )

}


# Create var.az_count public subnets, each in a different AZ
resource "aws_subnet" "public" {
  count = var.az_count

  vpc_id                  = aws_vpc.main.id
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  cidr_block              = element(var.public_subnets_cidr, count.index)
  map_public_ip_on_launch = "true"

  tags = merge(
    var.default_tags,
    tomap(
      {
        "Name"        = "${var.project_name}-public-subnet-${count.index}-${var.environment}",
        "type"        = "public",
        "environment" = var.environment
      }
    )
  )
}
