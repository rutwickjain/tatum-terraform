output "vpc_id" {
  value       = aws_vpc.main.id
  description = "VPC ID"
}

output "public_subnets_ids" {
  value       = aws_subnet.public.*.id
  description = "List of public subnets id"
}

output "private_subnets_ids" {
  value       = aws_subnet.private.*.id
  description = "List of private subnets id"
}
