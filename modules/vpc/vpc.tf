# Create VPC
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr

  #DNS Related Entries
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    var.default_tags,
    tomap(
      {
        "name"        = "${var.project_name}-vpc-${var.environment}",
        "environment" = "${var.environment}"
      }
    )
  )
}
