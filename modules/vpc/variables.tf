# variables.tf

// Environement

variable "environment" {
}

variable "project_name" {}

variable "az_count" {
  description = "Number of AZs to cover in a given region"
  default     = "3"
}

variable "vpc_cidr" {
  description = "CIDR Block for VPC"
}

variable "public_subnets_cidr" {
  description = "CIDR Blocks for public subnets"
  type        = list(any)
}

variable "private_subnets_cidr" {
  description = "CIDR Blocks for private subnets"
  type        = list(any)
}

variable "default_tags" {
  description = "Default tags for all resources"
  type        = map(any)
}

