# Internet Gateway for the public subnet
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    var.default_tags,
    tomap(
      {
        "Name"       = "${var.project_name}-internet-gateway-${var.environment}",
        "enviroment" = var.environment
    })
  )
}


# Create a NAT gateway with an Elastic IP for private subnet internet connectivity
resource "aws_eip" "eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.igw]
  tags = merge(
    var.default_tags,
    tomap(
      {
        "enviroment" = var.environment,
        "Name"       = "${var.project_name}-eip-${var.environment}"
    })
  )
}

resource "aws_nat_gateway" "ngw" {
  count         = "1"
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  allocation_id = aws_eip.eip.id
  tags = merge(
    var.default_tags,
    tomap(
      {
        "Name"       = "${var.project_name}-nat-gateway-${count.index}-${var.environment}",
        "enviroment" = var.environment
    })
  )
}
