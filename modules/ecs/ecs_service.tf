// create ECS service for python-app
resource "aws_ecs_service" "python-app-service" {
  name             = "${var.project_name}-${var.product_name}-${var.environment}"
  cluster          = aws_ecs_cluster.main.id
  task_definition  = aws_ecs_task_definition.app_definition.arn
  desired_count    = var.desired_count
  launch_type      = var.launch_type
  platform_version = var.platform_version

  lifecycle {
    ignore_changes = [desired_count, task_definition]
  }

  network_configuration {
    subnets          = var.private_subnets_ids
    security_groups  = [aws_security_group.ecs_task.id]
    assign_public_ip = var.assign_public_ip
  }
}
