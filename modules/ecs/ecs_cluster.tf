// create ECS cluster
resource "aws_ecs_cluster" "main" {
  name = "${var.project_name}-cluster-${var.environment}"

  setting {
    name  = "containerInsights"
    value = var.container_insights
  }

  tags = merge(
    var.default_tags,
    tomap(
      {
        "name"        = "${var.project_name}-cluster-${var.environment}",
        "environment" = "${var.environment}"
      }
    )
  )
}
