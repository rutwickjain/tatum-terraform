// General
variable "project_name" {}
variable "product_name" {}
variable "default_tags" {
  description = "Default tags for all resources"
  type        = map(any)
}

// Environment
variable "environment" {}
variable "region" {}

//vpc
variable "vpc_id" {}
variable "private_subnets_ids" {}
variable "public_subnets_ids" {}

//S3
variable "s3_bucket_name" {
  default = ""
}

//ECS Cluster
//variable "cluster_name" {}

variable "container_insights" {
  default = "enabled"
}

// ECS Service
variable "desired_count" {}
variable "assign_public_ip" {}
variable "platform_version" {}
variable "launch_type" {}

//ECS Task Defination
variable "container_cpu" {}
variable "container_memory" {}
variable "docker_image" {}
variable "container_port" {}
variable "ecs_container_cloudwatch_loggroup" {}

// AutoScaling
variable "enable_autoscaling" {
  default = "false"
}

variable "max_capacity" {
  default = "4"
}

variable "min_capacity" {
  default = "2"
}
