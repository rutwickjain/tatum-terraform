resource "aws_security_group" "ecs_task" {
  vpc_id = var.vpc_id

  tags = merge(
    var.default_tags,
    tomap(
      {
        "Name"        = "${var.project_name}-${var.environment}-ecs-sg",
        "environment" = "${var.environment}"
      }
    )
  )
}

resource "aws_security_group_rule" "ecs_egress_outbound_rule" {
  description       = "This is egress rule"
  type              = "egress"
  security_group_id = aws_security_group.ecs_task.id
  from_port         = -1
  to_port           = -1
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "ingress_outbound_rule" {
  description       = "This is egress rule"
  type              = "ingress"
  security_group_id = aws_security_group.ecs_task.id
  from_port         = var.container_port
  to_port           = var.container_port
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

