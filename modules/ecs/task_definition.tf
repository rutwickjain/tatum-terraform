locals {
  name = "${var.project_name}-${var.product_name}-${var.environment}"
}

resource "aws_ecs_task_definition" "app_definition" {
  family = local.name

  network_mode = "awsvpc"
  requires_compatibilities = [
    "FARGATE"
  ]

  cpu    = var.container_cpu
  memory = var.container_memory

  execution_role_arn = aws_iam_role.ecs_task_execution_role.arn
  task_role_arn      = aws_iam_role.ecs_task_role.arn

  container_definitions = <<TASK_DEFINITION
    [
      {
        "name": "${local.name}",
        "image": "${var.docker_image}",
        "cpu": ${var.container_cpu},
        "memory": ${var.container_memory},
        "environment": [
          {
            "name": "s3_bucket_name",
            "value": "${var.s3_bucket_name}"
          }
        ],
        "essential": true,
        "logConfiguration": {
          "logDriver": "awslogs",
          "options": {
            "awslogs-group": "${var.ecs_container_cloudwatch_loggroup}",
            "awslogs-region": "${var.region}",
            "awslogs-stream-prefix": "/aws/ecs"
          }
        }
      }
    ]
  TASK_DEFINITION


  tags = merge(
    var.default_tags,
    tomap(
      {
        "name"        = local.name,
        "environment" = var.environment
    })
  )

}
