locals {
  name = "${var.project_name}-${var.product_name}"
}

resource "aws_cloudwatch_log_group" "ecs_container_cloudwatch_log_group" {
  name              = "/aws/ecs/${local.name}-${var.environment}-container-logs"
  retention_in_days = var.retention_in_days

  tags = merge(
    var.default_tags,
    tomap(
      {
        "Name"        = "${local.name}-${var.environment}",
        "environment" = var.environment
    })
  )
}

resource "aws_cloudwatch_log_stream" "ecs_container_cloudwatch_logstream" {
  name           = "${local.name}-${var.environment}-container-log-stream"
  log_group_name = aws_cloudwatch_log_group.ecs_container_cloudwatch_log_group.name
}