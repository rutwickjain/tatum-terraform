variable "project_name" {}
variable "product_name" {}
variable "environment" {}
variable "retention_in_days" { default = 7 }
variable "default_tags" {}
