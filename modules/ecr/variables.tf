variable "ecr_repos_list" {
  default = []
}

variable "environment" {}

variable "default_tags" {
  description = "Default tags for all resources"
  type        = map(any)
}
