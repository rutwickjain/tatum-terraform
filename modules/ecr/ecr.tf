// Create ECR repos for provided list of names
resource "aws_ecr_repository" "repository" {
  for_each             = toset(var.ecr_repos_list)
  name                 = each.key
  image_tag_mutability = "IMMUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }

  tags = merge(
    var.default_tags,
    tomap(
      {
        "name"        = each.key,
        "environment" = "${var.environment}"
      }
    )
  )
}
