data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "document" {
  statement {
    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
      ]
    }
    actions = [
      "ecr:UploadLayerPart",
      "ecr:SetRepositoryPolicy",
      "ecr:PutImage",
      "ecr:ListImages",
      "ecr:InitiateLayerUpload",
      "ecr:GetRepositoryPolicy",
      "ecr:GetDownloadUrlForLayer",
      "ecr:DescribeRepositories",
      "ecr:DescribeImages",
      "ecr:DescribeImageScanFindings",
      "ecr:DeleteRepositoryPolicy",
      "ecr:DeleteRepository",
      "ecr:CompleteLayerUpload",
      "ecr:BatchGetImage",
      "ecr:BatchDeleteImage",
      "ecr:BatchCheckLayerAvailability",
    ]
  }
}

// create ECR IAM policy
resource "aws_ecr_repository_policy" "policy" {
  for_each   = toset(var.ecr_repos_list)
  repository = each.key
  policy     = data.aws_iam_policy_document.document.json
  depends_on = [
    aws_ecr_repository.repository
  ]
}
